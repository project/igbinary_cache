<?php

/**
 * @file
 *
 * Administrative functions for Igbinary Cache integration.
 */

/**
 * Menu callback for igbinary cache admin settings.
 */
function igbinary_cache_admin_settings_form() {
  $form = array();

  $form['igbinary_cache_enable'] = array(  
    '#type' => 'checkbox',
    '#title' => t('Enable'),
    '#default_value' => variable_get('igbinary_cache_enable', 0),
  );

  $form['#submit'][] = 'igbinary_cache_admin_settings_form_submit';

  return system_settings_form($form);
}

function igbinary_cache_admin_settings_form_submit($form, &$form_state) {

   drupal_flush_all_caches();
}
