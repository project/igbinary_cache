<?php

/**
 * @file
 * Cache include file, to be used in settings.php file.
 */

/**
 * Igbinary cache implementation.
 */
class IgbinaryCache extends DrupalDatabaseCache {

  /**
   * Prepares a cached item.
   *
   * Checks that items are either permanent or did not expire, and unserializes
   * data as appropriate.
   *
   * @param object $cache
   *   An item loaded from cache_get() or cache_get_multiple().
   *
   * @return object
   *   The item with data unserialized as appropriate or FALSE if there is no
   *   valid item to load.
   */
  protected function prepareItem($cache) {
    global $user;

    if (!isset($cache->data)) {
      return FALSE;
    }

    if ($cache->expire != CACHE_PERMANENT && variable_get('cache_lifetime', 0) && isset($_SESSION['cache_expiration'][$this->bin]) && $_SESSION['cache_expiration'][$this->bin] > $cache->created) {
      return FALSE;
    }

    if ($cache->serialized) {
      if (variable_get('igbinary_cache_enable', 0)) {
        $cache->data = igbinary_unserialize($cache->data);
      } else {
        $cache->data = unserialize($cache->data);
      }
    }

    return $cache;
  }

  /**
   * Implements DrupalCacheInterface::set().
   */
  public function set($cid, $data, $expire = CACHE_PERMANENT) {
    $fields = array(
      'serialized' => 0,
      'created' => REQUEST_TIME,
      'expire' => $expire,
    );
    if (!is_string($data)) {
      if (variable_get('igbinary_cache_enable', 0)) {
        $fields['data'] = igbinary_serialize($data);
      } else {
        $fields['data'] = serialize($data);
      }
      $fields['serialized'] = 1;
    }
    else {
      $fields['data'] = $data;
      $fields['serialized'] = 0;
    }

    try {
      db_merge($this->bin)
        ->key(array('cid' => $cid))
        ->fields($fields)
        ->execute();
    }
    catch (Exception $e) {
      // The database may not be available, so we'll ignore cache_set requests.
    }
  }

}
