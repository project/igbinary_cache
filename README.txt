/*****************
 * 1. Installation
 ****************/
- Step 1
Enable the module and make sure the Igbinary extension is 
installed properly on the status page (http://yoursite/admin/reports/status).

- Step 2
Add the following code to your settings.php file:

$conf['cache_backends'] = array('sites/all/modules/igbinary_cache/igbinary_cache.inc');
$conf['cache_default_class'] = 'IgbinaryCache';

- Step 3
Make sure you enable igbinary cache 
igbinary cache page (http://yoursite/admin/config/development/igbinary).

- Step 4
Visit your site to see or it's still working!
